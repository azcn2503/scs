$(document).ready( function(){

	function loadTemplates(){
	
		return $.Mustache.load('./templates/scs.addcampaign.php');
	
	}
	
	var Campaign = function(){
	
		var parent = this;

		this.form = null;
		this.campaign = null;
		this.headers = null;
		this.postdata = null;
		this.regex = null;
		this.target = null;
		this.url = null;
		
		this.FormButtonToggle = function(type){
		
			$(this.form).find('input[type=submit]').prop('disabled', type);
			
		};
		
		this.Start = function(){
		
			templates.done( function(){
				parent.target.mustache('download');
				parent.target.find('#download').fadeIn();
				parent.target.find('#download form').submit( function(){
					parent.form = this;
					parent.FormButtonToggle('disable');
					parent.url = this.elements['url'].value;
					parent.headers = this.elements['headers'].value;
					parent.Test();
					return false;
				});
			});
			
		};
		
		this.Test = function(){

			var postData = {type: 'test', url: this.url, headers: this.headers};
		
			$.ajax({ url: 'scs.api.php', data: postData, type: 'POST', cache: false, success: function(data){
				parent.FormButtonToggle(null);
				parent.campaign = data.campaign;
				parent.target.find('#downloaded-form-data, #captcha-form, #captcha-result, #campaign-code').remove();
				parent.target.mustache('downloaded-form-data', data);
				parent.target.find('#downloaded-form-data').fadeIn();
				location = '#downloaded-form-data';
				parent.target.find('#downloaded-form-data form').submit( function(){
					parent.form = this;
					parent.FormButtonToggle('disable');
					parent.postdata = $(this).serialize();
					parent.GetCaptcha();
					return false;
				});
			}});
		
		};

		this.GetCampaignCode = function(){

			var postData = {type: 'getcampaigncode', campaign: this.campaign, regex: this.regex};

			$.ajax({ url: 'scs.api.php', type: 'POST', data: postData, cache: false, success: function(data){
				parent.FormButtonToggle(null);
				parent.target.find('#campaign-code').remove();
				parent.target.mustache('campaign-code', data);
				parent.target.find('#campaign-code').fadeIn();
				location = '#campaign-code';
			}});

		};
		
		this.GetCaptcha = function(){

			var postData = {'type': 'getcaptcha', url: this.url, campaign: this.campaign, 'postdata': this.postdata};
		
			$.ajax({ url: 'scs.api.php', data: postData, type: 'POST', cache: false, success: function(data){
				parent.FormButtonToggle(null);
				var request = this;
				parent.target.find('#captcha-form, #captcha-result, #campaign-code').remove();
				parent.target.mustache('captcha-form', data);
				parent.target.find('#captcha-form').fadeIn();
				location = '#captcha-form';
				var $form = parent.target.find('#captcha-form form');
				var $submits = $form.find('input[type=submit]');
				request.submitType = null;
				$submits.click( function(){ request.submitType = this.name; });
				parent.target.find('#captcha-form form').submit( function(){
					parent.form = this;
					parent.FormButtonToggle('disable');
					if(request.submitType == 'retry'){ parent.GetCaptcha(); return false; }
					var elements = this.elements;
					var response = elements['response'].value;
					var key = elements['key'].value;
					var challenge = elements['challenge'].value;
					parent.SubmitCaptcha(response);
					return false; 
				});
			}});
		
		};
		
		this.SubmitCaptcha = function(response){
		
			var postData = {'type': 'submitcaptcha', 'campaign': this.campaign, 'response': response, 'postdata': this.postdata};
		
			$.ajax({ url: 'scs.api.php', data: postData, type: 'POST', cache: false, success: function(data){
				parent.FormButtonToggle(null);
				parent.target.find('#captcha-result, #campaign-code').remove();
				parent.target.mustache('captcha-result', data);
				parent.target.find('#captcha-result').fadeIn();
				parent.target.find('#captcha-result form').submit( function(){
					parent.form = this;
					parent.FormButtonToggle('disable');
					parent.regex = this.elements['regex'].value;
					parent.GetCampaignCode();
					return false;
				});
				location = '#captcha-result';
			}});
		
		};
	
	};

	$.when(templates = loadTemplates()).then( function(){
	
		var scs = new Campaign();
		scs.target = $('#wrapper');
		
		scs.Start();
		
	});
	
});

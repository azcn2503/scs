var SiteApp = function(){

	var parent = this;

	this.FancyForms = function(){

		$('label.input').on('focusin', function(event){
			var $span = $(this).find('span');
			$span.fadeOut();
		});

		$('label.input').on('focusout', function(event){
			var input = $(this).find('input').prop('value');
			if(input != ''){ return false; }
			var $span = $(this).find('span');
			$span.fadeIn();
		});

	};

	this.Login = function($form){
		var postData = $form.serialize();
		$.ajax({ url: 'ajax/login.php', data: postData, type: 'POST', success: function(data){
			console.log(data);
		}});
	};

	this.Register = function($form){
		var postData = $form.serialize();
		$.ajax({ url: 'ajax/register.php', data: postData, type: 'POST', success: function(data){
			console.log(data);
		}});
	};

	this.Start = function(){

		templates.done( function(){

			parent.$userbar.fadeIn().mustache('userbar', {});
			parent.$newsbar.fadeIn().mustache('newsbar', {});
			parent.FancyForms();

			parent.$userbar.find('form[name=login]').submit( function(){
				parent.Login($(this));
				return false;
			});
			parent.$userbar.find('form[name=register]').submit( function(){
				parent.Register($(this));
				return false;
			});

		});

	};

};
<!DOCTYPE html>

<html>

	<head>
	
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1, user-scalable=no" />
		
		<title>SCS - Social Captcha Service - Campaign Creation</title>
		
		<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.mustache.js"></script>
		<script type="text/javascript" src="js/mustache.js"></script>
		<script type="text/javascript" src="js/scs.addcampaign.js"></script>

		<link type="text/css" rel="stylesheet" href="scs.css" />
		
	</head>
	
	<body>

		<div id="wrapper" class="content bordered lightbg shadowed"></div>
	
	</body>
	
</html>

<?php

	header('content-type: application/json');

	require_once('../db.class.php');

	$response = array(
		'data' => array(),
		'error' => '',
		'status' => ''
	);

	while(1){

		if(!isset($_POST)){ break; }

		extract($_POST);

		if(!isset($username, $emailaddress, $password, $confirmpassword) || empty($username) || empty($emailaddress) || empty($password) || empty($confirmpassword)){ $response['error'] = 'Missing stuff'; break; }

		if($password != $confirmpassword){ $response['error'] = 'Passwords no match'; break; }

		break;

	}

	echo(json_encode($response));

?>
<?php

	header("content-type: application/json");

	require_once("./db.class.php");
	require_once("./scs.class.php");

	$response = array();

	while(1){

		if(isset($_POST['type'])){
		
			$type = $_POST['type'];
			
			if($type == 'test'){
			
				// Test for presence of a captcha form
				
				if(!isset($_POST['url'])){ break; }
				$url = $_POST['url'];

				$SCS = new SCS();

				// Set headers
				if(isset($_POST['headers'])){
					$headers = $_POST['headers'];
					$headers = explode("\n", $headers);
					$headerString = "";
					foreach($headers as $header){
						$header = trim($header);
						if($header == ''){ continue; }
						$headerString.= "--header \"{$header}\" ";
					}
					$SCS->SetOptions('headers', $headerString);
				}
				
				// Test
				$response = $SCS->Test($url);
				
			}

			if($type == 'getcaptcha'){
			
				// Download a captcha form
				
				if(!isset($_POST['url'], $_POST['campaign'])){ break; }
				$url = $_POST['url'];
				$campaign = $_POST['campaign'];

				$SCS = new SCS();

				// Set options
				/*if(isset($_POST['postdata'])){
					$postdata = $_POST['postdata'];
					$SCS->SetOptions('postdata', $postdata);
				}*/
				$SCS->SetOptions('campaign', $campaign);
				
				// Get captacha
				$response = $SCS->GetCaptcha($url);
				
			}
			
			if($type == 'submitcaptcha'){
			
				// Submit a captcha response
				
				if(!isset($_POST['campaign'], $_POST['response'])){ break; }
				$campaign = $_POST['campaign'];
				$response = $_POST['response'];
				
				$SCS = new SCS();

				$SCS->SetOptions('campaign', $campaign);
				if(isset($_POST['postdata']) && $_POST['postdata'] != ''){ $SCS->SetOptions('postdata', $_POST['postdata']); }
				
				$response = $SCS->SubmitCaptcha($campaign, $response);
				
			}

			if($type == 'getcampaigncode'){

				if(!isset($_POST['campaign'], $_POST['regex'])){ break; }

				$SCS = new SCS();
				$SCS->SetOptions('regex', $_POST['regex']);
				$response = $SCS->GetCampaignCode($_POST['campaign']);

			}
		
		}

		if(isset($_POST['url'], $_POST['regex'])){

			// Set up a new campaign

			$SCS = new SCS();

			if(isset($_POST['headers'])){ $SCS->SetOptions('headers', $_POST['headers']); }
			$test = $SCS->Test($_POST['url'], $_POST['regex']);

			$response = $test;

		}

		if(isset($_GET['type'])){

			if($_GET['type'] == 'campaign'){

				// Register a new campaign

				$SCS = new SCS();
				if(isset($_GET['regex'])){ $SCS->SetOptions('regex', $_GET['regex']); }
				if(isset($_GET['postdata'])){ $SCS->SetOptions('postdata', $_GET['postdata']); }
				if(isset($_GET['headers'])){ $SCS->SetOptions('headers', $_GET['headers']); }
				$SCS->SetOptions('ready', 1);
				$response = $SCS->Test($_GET['url']);

			}

			if($_GET['type'] == 'captchaform'){

				// Load the captcha form

				$response = array('status' => 0, 'template' => '');

				$SCS = new SCS();

				$get = $SCS->Get();

				while(1){

					if($get['status'] == 0){ $template = file_get_contents('scs.template.nocaptchas.php'); }
					else{
						$template = file_get_contents('scs.template.captchaform.php');
						$patterns = array("/%campaign%/m", "/%image%/m");
						$replacements = array($get['data']['id'], $get['data']['image']);
						$template = preg_replace($patterns, $replacements, $template);
						$response['status'] = 1;
					}

					$response['template'] = $template;

					break;

				}

			}

			if($_GET['type'] == 'respond'){

				// Respond to the captcha form

				$SCS = new SCS();

				if(isset($_GET['headers'])){ $SCS->SetOptions('headers', $_GET['headers']); }
				$SCS->SetOptions('ready', 1);
				$respond = $SCS->SubmitCaptcha($_GET['campaign'], $_GET['response']);

				$response = $respond;

			}

			if($_GET['type'] == 'stats'){

				// Daily and live stats
				
				$SCS = new SCS();
				
				$response = array('daily' => array(), 'live' => array());
				
				$response['daily'] = $SCS->GetStats('daily');
				$response['live'] = $SCS->GetStats('live');
				
			}

			if($_GET['type'] == 'status'){

				// Get status of captcha

				if(isset($_GET['campaign'])){

					$SCS = new SCS();

					$response = $SCS->GetStatus($_GET['campaign']);
					$response['status_text'] = call_user_func(function($status){
						if($status == 1){ return 'Queued'; }
						if($status == 2){ return 'Dispatched'; }
						if($status == 3){ return 'Success'; }
						if($status == 4){ return 'Failure'; }
						if($status == 5){ return 'Expired'; }
						return 'Waiting';
					}, $response['status']);

				}

			}

		}

		break;

	}

	echo(json_encode($response));

?>

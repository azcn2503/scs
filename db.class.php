<?php

    class ConnectionException extends Exception { }
    class CreateDatabaseException extends Exception { }
    class CreateTableException extends Exception { }

    class Database {

        private $link = null;

        public function Connect() {

            $link = new mysqli('localhost', 'scsuser', 'scspass');
            if($link->connect_errno) {
                throw new ConnectionException('Error connecting to MySQL (' . $link->connect_errno . '): ' . $link->connect_error, 1);
            }
            $this->link = $link;
            if(!$link->select_db('scs')) {
                $this->Create();
            }

            return $this->link;

        }

        private function Create() {

            $this->link->query('CREATE DATABASE scs');
            if($this->link->errno > 0) {
                throw new CreateDatabaseException('Error creating SCS database (' . $this->link->errno . '): ' . $this->link->error, 1);
            }

            $this->link->select_db('scs');

            $this->link->query('CREATE TABLE `scs` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `status` int(11) DEFAULT NULL,
                `ready` int(11) DEFAULT NULL,
                `url` text,
                `regex` text,
                `keystring` text,
                `challenge` text,
                `image` text,
                `data` text,
                `headers` text,
                `cookies` text,
                `action` text,
                `method` text,
                `response` text,
                `tries` int(11) DEFAULT NULL,
                `html` text,
                `datetime_enter` datetime DEFAULT NULL,
                `datetime_queue` datetime DEFAULT NULL,
                `datetime_dispatch` datetime DEFAULT NULL,
                `datetime_response` datetime DEFAULT NULL,
                `seconds_total` int(11) DEFAULT NULL,
                `seconds_setup` int(11) DEFAULT NULL,
                `seconds_queue` int(11) DEFAULT NULL,
                `seconds_user` int(11) DEFAULT NULL,
                `server_action` varchar(10) DEFAULT NULL,
                PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8');
            if($this->link->errno > 0) {
                throw new CreateTableException('Error creating SCS table (' . $this->link->errno . '): ' . $this->link->error, 1);
            }

            return true;

        }

    }

?>
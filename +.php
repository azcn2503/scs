<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />

		<meta name="viewport" content="initial-scale=1, user-scalable=no" />

		<title>SCS</title>

		<script src="js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.mustache.js"></script>
		<script type="text/javascript" src="js/mustache.js"></script>
		<script type="text/javascript" src="js/scs.site.js"></script>

		<script type="text/javascript">

			$(document).ready( function(){

				function loadTemplates(){
	
					return $.Mustache.load('./templates/scs.site.php');
				
				}

				$.when(templates = loadTemplates()).then( function(){
	
					var scs = new SiteApp();
					scs.$userbar = $('#userbar');
					scs.$newsbar = $('#newsbar');
					$('#wrapper').fadeIn();
					scs.Start();
					
				});

			});

		</script>
		
		<link type="text/css" rel="stylesheet" href="scs.css" />

	</head>

	<body>

		<div id="userbar" class="darkbg dn"></div>
		<div id="newsbar" class="darkbg dn"></div>

		<div id="wrapper" class="content bordered dn lightbg shadowed">
			<div class="content tac">
				<h3>Welcome to the Social Captcha Service</h3>
				<p title="Hey, this sounds familiar!">Coming soon...</p>
			</div>
		</div>

	</body>

</html>

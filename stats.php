<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />

		<meta name="viewport" content="initial-scale=1, user-scalable=no" />

		<title>SCS - Social Captcha Service - Complete Captchas for Cash</title>

		<script src="js/jquery-1.9.1.min.js"></script>
		
		<script type="text/javascript">
		
			function stats(){
				$.ajax({ url: 'scs.api.php', data: 'type=stats', type: 'GET', cache: false, timeout: 2000, success: function(data){
					$('#waiting').html('<h1>' + data.live.status_waiting + '</h1><h3>Waiting</h3>');
					$('#queued').html('<h1>' + data.live.status_queued + '</h1><h3>Queued</h3>');
					$('#dispatched').html('<h1>' + data.live.status_dispatched + '</h1><h3>Dispatched</h3>');
					$('#daily').html('<h1>' + data.live.status_completed + ' @ ' + Math.round(data.daily.seconds_user_avg * 10) / 10 + 's</h1>');
				}});
				setTimeout( function(){ stats(); }, 2000);
			}
			
			$(document).ready( function(){
				stats();
			});
		
		</script>
		
		<style type="text/css">
		
			body {
				font-size: 2em;
			}
		
			table {
				position: absolute;
				left: 0;
				top: 0;
				width: 100%;
				height: 100%;
				table-layout: fixed;
			}
			
			td {
				text-align: center;
				vertical-align: middle;
			}
			
			h1, h3 {
				margin: 0;
			}
		
		</style>

	</head>

	<body>

		<table>
			<tr>
				<td><div id="waiting"></div></td>
				<td><div id="queued"></div></td>
				<td><div id="dispatched"></div></td>
			</tr>
			<tr>
				<td colspan="3"><div id="daily"></div></td>
			</tr>
		</table>

	</body>

</html>

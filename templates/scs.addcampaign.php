<?php
	function getHeader($name, $type){
		if(isset($_SERVER[$type]) && $_SERVER[$type] != ''){
			return $name . ": " . $_SERVER[$type] . "\n";
		}
		return '';
	}
	$headers = getHeader('Accept', 'HTTP_ACCEPT')
	. getHeader('Accept-Charset', 'HTTP_ACCEPT_CHARSET')
	//. getHeader('Accept-Encoding', 'HTTP_ACCEPT_ENCODING')
	. getHeader('Accept-Language', 'HTTP_ACCEPT_LANGUAGE')
	. getHeader('Cache-Control', 'HTTP_CACHE_CONTROL')
	. getHeader('Connection', 'HTTP_CONNECTION')
	. getHeader('User-Agent', 'HTTP_USER_AGENT');
	$headers = trim($headers);
?>
<script type="text/html" id="no-captcha">
	<div class="content tac">No Captcha detected. Try again?</div>
</script>
<script type="text/html" id="campaign-code">
	<div id="campaign-code" class="dn">
		<a name="campaign-code"></a>
		{{#match}}
			<div class="content tac">Your expression matched content on the page.</div>
		{{/match}}
		{{^match}}
			<div class="content tac">
				Your expression did not match content on the page.<br />
				You don't need to provide a regular expression to register the campaign, but it is strongly advised that you do.
			</div>
		{{/match}}
		<div class="content">
			<table class="wf">
				<tr>
					<td>Campaign Code:</td>
					<td><textarea class="wf">{{code}}</textarea></td>
				</tr>
			</table>
		</div>
	</div>
</script>
<script type="text/html" id="captcha-form">
	<div id="captcha-form" class="dn">
		<a name="captcha-form"></a>
		{{#status}}
			<div class="content tac">Please respond to the captcha below.</div>
			<div>
				<form>
					<input type="hidden" name="key" value="{{key}}" />
					<input type="hidden" name="challenge" value="{{challenge}}" />
					<table class="wf">
						<tr>
							<td class="tac"><img src="data:image/jpeg;base64,{{image}}" alt="Captcha" /></td>
						</tr>
						<tr>
							<td class="tac"><input type="text" name="response" class="tac" style="width: 300px;" /></td>
						</tr>
						<tr>
							<td class="tac"><input type="submit" name="submit" value="Submit Captcha"/> or <input type="submit" name="retry" value="Retry" /></td>
						</tr>
					</table>
				</form>
			</div>
		{{/status}}
		{{^status}}
			{{>no-captcha}}
		{{/status}}
	</div>
</script>
<script type="text/html" id="captcha-result">
	<div id="captcha-result" class="dn">
		<a name="captcha-result"></a>
		{{#status}}
			<div class="content tac">The page has been downloaded.<br />You may want to match the results against a regular expression.</div>
			<form>
				<table class="wf">
					<tr>
						<td>HTML:</td>
						<td><textarea name="html" class="wf">{{html}}</textarea></td>
					</tr>
					<tr>
						<td>Regex:</td>
						<td><input type="text" name="regex" class="wf" value="/\bcorrect\b/i" /></td>
					</tr>
					<tr>
						<td colspan="2" class="tac"><input type="submit" value="Match and Generate Campaign Code" /></td>
					</tr>
				</table>
			</form>
		{{/status}}
		{{^status}}
			<div class="content tac">There was an error downloading the page.<br />Perhaps try different headers?</div>
		{{/status}}
	</div>
</script>
<script type="text/html" id="download">
	<div id="download" class="dn">
		<a name="download"></a>
		<form>
			<table class="wf">
				<tr>
					<td>URL:</td>
					<td><input type="text" name="url" value="http://www.google.com/recaptcha/demo/" class="wf" /></td>
				<tr>
					<td>Headers:</td>
					<td><textarea name="headers" class="wf"><?php echo(htmlentities($headers)); ?></textarea></td>
				</tr>
				<tr>
					<td colspan="2" class="tac"><input type="submit" value="Test this URL" /> or <input type="reset" value="Reset" /></td>
				</tr>
			</table>
		</form>
	</div>
</script>
<script type="text/html" id="downloaded-form-data">
	<div id="downloaded-form-data" class="dn">
		<a name="downloaded-form-data"></a>
		{{#status}}
			<div class="content tac">Captcha form detected.<br />Captcha will be submitted using {{captchaform.method}} to {{captchaform.action}}</div>
			<form>
				<table class="wf">
					{{#captchaform.data}}{{#template_valid}}
						<tr>
							<td>{{name}}</td>
							<td><span class="info">{{type}}</span></td>
							<td>
								{{#template_text}}<input type="text" name="{{name}}" value="{{value}}" class="wf" />{{/template_text}}
								{{#template_file}}<input type="file" name="{{name}}" />{{/template_file}}
							</td>
						</tr>
					{{/template_valid}}{{/captchaform.data}}
					<tr>
						<td colspan="3" class="tac"><input type="submit" value="Proceed to Captcha" /> or <input type="reset" value="Reset" /></td>
					</tr>
				</table>
			</form>
		{{/status}}
		{{^status}}
			{{>no-captcha}}
		{{/status}}
	</div>
</script>

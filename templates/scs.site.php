<script type="text/html" id="userbar">
	<div class="content">
		<h3>Log in to SCS</h3>
		<form name="login">
			<table class="wf">
				<tr>
					<td><label class="input"><span>Username</span><input type="text" name="username" class="wf" title="Username" /></label></td>
				</tr>
				<tr>
					<td><label class="input"><span>Password</span><input type="password" name="password" class="wf" title="Password" /></label></td>
				</tr>
				<tr>
					<td class="tac"><input type="submit" value="Log in" title="Log in" /></td>
				</tr>
			</table>
		</form>
		<h3>Register</h3>
		<form name="register">
			<table class="wf">
				<tr>
					<td><label class="input"><span>Username</span><input type="text" name="username" class="wf" title="Username" /></label></td>
				</tr>
				<tr>
					<td><label class="input"><span>Email address</span><input type="text" name="emailaddress" class="wf" title="Email address" /></label></td>
				</tr>
				<tr>
					<td><label class="input"><span>Password</span><input type="password" name="password" class="wf" title="Password" /></label></td>
				</tr>
				<tr>
					<td><label class="input"><span>Confirm password</span><input type="password" name="confirmpassword" class="wf" title="Confirm password" /></label></td>
				</tr>
				<tr>
					<td class="tac"><input type="submit" value="Register" title="Register" /></td>
				</tr>
			</table>
		</form>
	</div>
</script>

<script type="text/html" id="newsbar">
	<div class="content">
		<h3>Welcome to SCS</h3>
		<p>SCS stands for 'Social Captcha Service' - it is a service aimed to automate the procedure of responding to Captcha's (currently only Recaptcha is supported).</p>
		<p>We are different from other Captcha solving services in that our server will manage the downloading of the webpages and Captcha images for you - no need to upload images or documents to us. The server will hold on to your request until it is completed.</p>
		<p>We provide a very simple and fast API to submit and check on the status of your Social Captcha Service campaign.</p>
		<p>For now, you can try our campaign generator, <a href="_.php">here</a>.</p>
	</div>
</script>
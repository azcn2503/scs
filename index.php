<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />

		<meta name="viewport" content="initial-scale=1, user-scalable=no" />

		<title>SCS - Social Captcha Service - Complete Captchas for Cash</title>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

		<script type="text/javascript">

			$(document).ready( function(){

				var SCS = function(target){

					this.target = target;
					this.timerTarget = "#scstimer";
					this.timeRemaining = 15;
					this.timerFn = null;

					this.Get = function(){

						var parent = this;

						$.ajax({ url: 'scs.api.php', type: 'GET', data: 'type=captchaform', cache: false, success: function(data){

							$(parent.target).hide().html(data.template).fadeIn();

							$(parent.target).find("form").submit( function(){
								parent.Respond(this);
								return false;
							});

							$(parent.target).find("input[name=response]").focus();

							parent.CancelTick();
							parent.Tick();

						}});

					};

					this.Respond = function(form){

						$(form).find('input').prop('disabled', 'disabled');

						this.CancelTick();

						var parent = this;

						var elements = form.elements;
						var campaign = elements['campaign'].value;
						var response = elements['response'].value;

						$.ajax({ url: 'scs.api.php', type: 'GET', data: "type=respond&campaign=" + campaign + "&response=" + response, cache: false, success: function(data){

							$.when(parent.Retry()).then(function(){ parent.Get(); });
							
						}});

					};

					this.Submit = function(postdata){

						$.ajax({ url: 'scs.api.php', type: 'POST', data: postdata, cache: false, success: function(data){
						}});

					};

					this.CancelTick = function(){

						this.timeRemaining = 15;
						clearTimeout(this.timerFn);

					};

					this.Tick = function(){

						var parent = this;

						this.timeRemaining--;

						if(this.timeRemaining >= 0){

							$(this.timerTarget).text(this.timeRemaining).fadeIn();

							this.timerFn = setTimeout(function(){ parent.Tick(); }, 1000);

						}

						else{

							$.when(this.Retry()).then(function(){ parent.Get(); });

						}

					};

					this.Retry = function(){

						return $(this.target).add(this.timerTarget).fadeOut(null, function(){ return true; });

					};

				};

				var scs = new SCS("#scs");
				scs.Get();
				// scs.Submit({url: 'http://www.google.com/recaptcha/demo/', regex: '/(\\bsuccess\\b|\\bjuist\\b)/i'});

			});

		</script>

		<style type="text/css">

			input[type=text] {
				box-sizing: border-box;
				-moz-box-sizing: border-box;
				width: 300px;
				padding: 5px;
			}

		</style>

	</head>

	<body>

		<table style="position: absolute; left: 0; top: 0; width: 100%; height: 100%;"><tr><td style="text-align: center; vertical-align: middle;">
			<div id="scs" style="padding: 10px; "></div>
			<div id="scstimer" style="padding: 10px; "></div>
		</table>

	</body>

</html>

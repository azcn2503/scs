<?php

	$args = array();
	if(sizeof($argv) < 1) { exit(); }
	foreach($argv as $arg) {
		if(!strpos($arg, '=')) { continue; }
		$arg = preg_split('/=/', $arg);
		if(sizeof($arg) < 1) { continue; }
		$k = $arg[0]; $v = $arg[1];
		$args[$k] = $v;
	}

	require_once("./db.class.php");
	require_once("./scs.class.php");

	$db = new Database();
	$db = $db->Connect();

	$res = $db->query('SELECT id, url, regex, tries, ready FROM scs WHERE id = ' . $args["campaign"]);
	
	while(1){

		if($res->num_rows == 0){ break; }

		$fld = $res->fetch_assoc();

		$SCS = new SCS();

		$SCS->SetOptions('ready', $fld['ready']);
		$SCS->SetOptions('campaign', $fld['id']);
		$SCS->SetOptions('regex', $fld['regex']);
		$test = $SCS->Test($fld['url']);

		break;

	}

?>

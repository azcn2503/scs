# Social Captcha Service

## Introduction

Social Captcha Service is a service and API that allows you to download webpages and solve the Captcha wall socially by using the built-in web interface. The general flow of SCS is:

 1. Page is downloaded and presence of Captcha is detected
 2. Captcha is dispatched to users for solving
     a. Expired and failed Captcha's are retried and requeued as necessary
 3. Resulting page is downloaded and contents matched against an expected expression to determine success/failure
 
As someone registering the campaign, you get to track the progress of your campaign through the various stages of waiting to process, being queued, being dispatched to the user, and seeing when the request fails or completes. You will see statistics showing how long it took between being logged, being dispatched and being solved successfully.

## Requirements

### Windows

 - PHP 5 (probably 5.4) or above must be installed and its directory added to your path
     - Download from here: http://windows.php.net/
     - Requires the following extensions: *php_mysqli.dll*, *php_com_dotnet.dll* and *php_curl.dll*
 - curl must be installed and its directory added to your path
     - Download from here: http://curl.haxx.se/download.html
 - MySQL Server must be installed
     - Must have the following username/password: scsuser/scspass
         - Must be allowed to create new databases on first run of SCS

### Linux

If running in Ubuntu/Debian, you can install all required utilities with the following command:

    sudo apt-get install php5 php5-cli php5-curl php5-mysql mysql-server
    
### Universal

You will need a web server configured to execute PHP scripts. Nginx is the top recommendation, followed by Apache, and lastly IIS for Windows, if it even supports running PHP scripts.
    
    
## Usage

 1. Start the server with `php scs.server.php`
 2. Browse to the `_.php` file to create a new campaign
 3. Browse to the `index.php` file to solve campaigns
 
You can also browse to `stats.php` for some generic overall statistics. Each campaign also provides a campaign URL that provides more detailed statistics specific for that campaign.
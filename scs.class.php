<?php

	class SCS {

		const __API__ = "http://www.google.com/recaptcha/api/";
		private $db = null;
		private $environment = 'linux';
		private $phpBin = 'php';
		private $shell = null;
		private $options = array('cookies' => '', 'headers' => '', 'postdata' => '', 'campaign' => false, 'regex' => '', 'ready' => 0);
		private $hostName = 'hostname';
		private $logLevel = 0;

		function __construct(){

			$this->DetectAndSetCapabilities();

			$db = new Database();
			try {
				$db = $db->Connect();
			}
			catch(Exception $e) {
				$this->LogMessage($e->getMessage());
				exit();
			}
			$this->db = $db;

		}

		function DetectAndSetCapabilities() {

			$cli = php_sapi_name();
			if($cli == 'cli') { $this->SetLogLevel(4); }

			$uname = php_uname();
			while(1) {
				if(preg_match('/^win/i', $uname)) { 
					$this->SetEnvironment('windows');
					break;
				}
				if(preg_match('/^linux/i', $uname)) {
					$this->SetEnvironment('linux');
					break;
				}
				$this->LogMessage('Sorry - this environment is not supported');
				exit();
			}

			$phpBins = array('php', 'php5', 'php5-cli');
			foreach($phpBins as $phpBin) {
				$command = $phpBin . ' -v';
				$output = shell_exec($command);
				if(preg_match('/^PHP/', $output)) {
					$this->phpBin = $phpBin;
					$this->LogMessage('phpbin is: ' . $phpBin);
					break;
				}
			}

			$this->hostName = gethostname();

		}

		function RunAsyncShellCommand($bin = '', $command = '') {

			if($bin == '' || $command == '') { return false; }

			if($this->environment == 'linux') {
				shell_exec('nohup ' . $bin . ' ' . $command . ' > /dev/null 2>&1 &');
			}
			if($this->environment == 'windows') {
				$this->shell->run($bin . ' ' . $command);
			}

			return true;

		}

		function SetEnvironment($environment) {

			$this->environment = $environment;

			if($environment == 'windows') {
				$this->shell = new COM('WScript.Shell');
			}

			$this->LogMessage('Set environment to: ' . $environment);

			return true;

		}

		function DeleteFile($file) {

			if($this->environment == 'windows') { 
				$file = preg_replace('/\//', '/\\/', $file);
			}

			if($this->environment == 'linux') {
				$command = 'rm -f "' . $file . '"';
			}
			if($this->environment == 'windows') {
				$command = 'del /f /q "' . $file . '"';
			}

			shell_exec($command);

		}

		function GetCampaignCode($campaign){

			// Get the campaign code for a specified campaign ID

			$response = array('match' => 0, 'code' => '');

			$campaign = $this->db->real_escape_string($campaign);

			$res = $this->db->query("SELECT * FROM scs WHERE id = '{$campaign}'");
			$fld = $res->fetch_assoc();

			if(preg_match($this->options['regex'], $fld['html']) == 1){ $response['match'] = 1; }

			$query = array(
				'type' => 'campaign',
				'url' => $fld['url'],
				'regex' => $this->options['regex'],
				'postdata' => $this->options['postdata'],
				'headers' => $fld['headers']
			);
			$query = http_build_query($query);
			$code = 'http://' . $this->hostName . '/scs.api.php?' . $query;

			$response['code'] = $code;

			return $response;

		}

		function GetCaptcha($url){
		
			// Download and return the Captcha form from the specified URL
			
			$response = array('html' => '', 'action' => $url, 'method' => 'GET', 'status' => 0);
			
			while(1){

				$html = $this->Download($url);
				$response['html'] = $html;
				
				//preg_match("/\<form(.*?)recaptcha(.*?)(\/form>|\/body>|\/html>)/i", $html, $forms);
				preg_match("/\<form((.|[\n\r])*?)recaptcha((.|[\n\r])*?)(\/form>|\/body>|\/html>)/i", $html, $forms);
				if(sizeof($forms) == 0){ break; }
				$form = $forms[0];

				// Match the form element
				preg_match("/\<form((.|[\n\r])*?)\>/im", $form, $form_element);
				if(sizeof($form_element) == 0){ break; }
				$form_element = $form_element[0];

				$form_data = $this->ProcessAttributes($form_element, array("action", "method", "name", "onsubmit"));

				if($form_data['action'] != ''){
				        $response['action'] = $form_data['action'];
				}
				if($form_data['method'] != ''){
				        $response['method'] = $form_data['method'];
				}

				// Match input as data
				while(1){

				        $input = array();
				        preg_match_all("/\<input((.|[\n\r])*?)\>/i", $form, $inputs);
				        if(sizeof($inputs) == 0){ break; }
				        $input = $inputs[0];

				        foreach($input as $element){
				                $response['data'][] = $this->ProcessAttributes($element, array("name", "type", "value"));
				        }

				        break;

				}

				preg_match("/(?<=challenge\?k=)[^\"]+/i", $form, $key);
				if(sizeof($key) == 0){ break; }
				$key = $key[0];

				$response['key'] = $key;

				$key_url = self::__API__ . "noscript?k={$key}";

				$noscript_html = $this->Download($key_url);
				preg_match("/(?<=src=\"image\?c=)[^\"]+/", $noscript_html, $src);
				if(sizeof($src) == 0){ break; }
				$src = $src[0];

				$response['challenge'] = $src;

				$src_url = self::__API__ . "image?c=" . $src;

				//$response['image'] = base64_encode(shell_exec("curl {$src_url}"));
				$response['image'] = base64_encode($this->Download($src_url));

				$response['status'] = 1;

				if(!$this->options['campaign']){ break; }

				$db_keystring = $this->db->real_escape_string($response['key']);
				$db_challenge = $this->db->real_escape_string($response['challenge']);
				$db_image = $this->db->real_escape_string($response['image']);
				$db_datetime_enter = date("Y-m-d H:i:s");

				$query = "UPDATE scs SET keystring = '{$db_keystring}', challenge = '{$db_challenge}', image = '{$db_image}', datetime_enter = '{$db_datetime_enter}' WHERE ready = 0 AND id = '" . $this->options['campaign'] . "'";

				$this->db->query($query);

				break;
			
			}
			
			return $response;
			
		}

		function Get(){

			// Get the stuff for the form

			$response = array('status' => 0, 'data' => array());

			// Re-queue a random old request, if necessary (allow server to keep up with influx of users)
			$res = $this->db->query('UPDATE scs SET server_action = "retry" WHERE status = 0 AND ready = 1 AND datetime_enter < NOW() - INTERVAL 30 SECOND ORDER BY RAND() LIMIT 1');

			// Queue a random captcha (Status 0 -> 1)
			$db_datetime_queue = date("Y-m-d H:i:s");
			$res = $this->db->query("UPDATE scs SET status = 1, datetime_queue = '{$db_datetime_queue}' WHERE status = 0 AND ready = 1 AND datetime_enter > NOW() - INTERVAL 30 SECOND ORDER BY RAND() LIMIT 1");

			// Grab a random captcha (Status 1)
			$res = $this->db->query("SELECT id, image FROM scs WHERE status = 1 AND ready = 1 AND datetime_queue > NOW() - INTERVAL 30 SECOND ORDER BY RAND() LIMIT 1");

			while(1){

				if($res->num_rows == 0){ break; }

				$fld = $res->fetch_assoc();

				$db_datetime = date("Y-m-d H:i:s");

				// Deal with captcha (Status 1 -> 2)
				$this->db->query("UPDATE scs SET status = 2, datetime_dispatch = '{$db_datetime}' WHERE id = '{$fld["id"]}'");

				$response['status'] = 1;
				$response['data'] = $fld;

				break;

			}

			return $response;

		}

		function GetStats($type){
		
			$response = array();
			
			if($type == 'daily'){
			
				//$res = $this->db->query("SELECT (SELECT COUNT(*) FROM scs AS tbl_total WHERE datetime_enter > NOW() - INTERVAL 24 HOUR) AS campaign_total, (SELECT COUNT(*) FROM scs AS tbl_ready WHERE ready = 1 AND datetime_enter > NOW() - INTERVAL 24 HOUR) AS campaign_ready FROM scs LIMIT 1");
				$res = $this->db->query("SELECT (SELECT COUNT(*) FROM scs AS tbl_total WHERE datetime_enter > NOW() - INTERVAL 24 HOUR) AS campaign_total, (SELECT COUNT(*) FROM scs AS tbl_ready WHERE ready = 1 AND datetime_enter > NOW() - INTERVAL 24 HOUR) AS campaign_ready,  (SELECT AVG(seconds_user) FROM scs AS tbl_seconds_user_avg WHERE datetime_response > NOW() - INTERVAL 24 HOUR) AS seconds_user_avg, (SELECT AVG(seconds_queue) FROM scs AS tbl_seconds_queue_avg WHERE datetime_response > NOW() - INTERVAL 24 HOUR) AS seconds_queue_avg, (SELECT AVG(seconds_setup) FROM scs AS tbl_seconds_setup_avg WHERE datetime_response > NOW() - INTERVAL 24 HOUR) AS seconds_setup_avg, (SELECT AVG(seconds_total) FROM scs AS tbl_seconds_total_avg WHERE datetime_response > NOW() - INTERVAL 24 HOUR) AS seconds_total_avg FROM scs LIMIT 1");
				$response = $res->fetch_assoc();
			
			}
			
			if($type == 'live'){
			
				$res = $this->db->query("SELECT (SELECT COUNT(*) FROM scs AS tbl_0 WHERE status = 0 AND ready = 1) AS status_waiting, (SELECT COUNT(*) FROM scs AS tbl_1 WHERE status = 1 AND ready = 1) AS status_queued, (SELECT COUNT(*) FROM scs AS tbl_2 WHERE status = 2 AND ready = 1) AS status_dispatched, (SELECT COUNT(*) FROM scs AS tbl_3 WHERE status = 3 AND ready = 1) AS status_completed, (SELECT COUNT(*) FROM scs AS tbl_4 WHERE status = 4 AND ready = 1) AS status_failed, (SELECT COUNT(*) FROM scs AS tbl_5 WHERE status = 5 AND ready = 1) AS status_expired FROM scs LIMIT 1");
				$response = $res->fetch_assoc();
			
			}
			
			return $response;
		
		}

		function GetStatus($campaign){

			// Find out the status

			$response = array('status' => 0);

			$campaign = $this->db->real_escape_string($campaign);

			$res = $this->db->query("SELECT response, status, datetime_enter, datetime_queue, datetime_dispatch, datetime_response, tries, html, seconds_total, seconds_setup, seconds_queue, seconds_user FROM scs WHERE id = '{$campaign}'");

			while(1){

				if($res->num_rows == 0){ break; }

				$fld = $res->fetch_assoc();

				$response = $fld;

				break;

			}

			return $response;

		}

		function SubmitCaptcha($campaign, $response_field){

			$response = array('status' => 0);

			$campaign = $this->db->real_escape_string($campaign);

			$res = $this->db->query("SELECT * FROM scs WHERE id = '{$campaign}'");
			$fld = $res->fetch_assoc();

			// Break post data down to array, add response to it, then build query again
			parse_str($fld['data'], $postdata);
			$postdata['recaptcha_response_field'] = $response_field;
			$postdata['recaptcha_challenge_field'] = $fld['challenge'];
			$postdata = "--request POST --data \"" . http_build_query($postdata) . "\"";

			$this->SetOptions('postdata', $postdata);
			$this->SetOptions('headers', $fld['headers']);

			$test = $this->Download($fld['action']);

			if($this->options['ready'] == 1){

				$regex = $fld['regex'];

				if($regex != ''){ preg_match($regex, $test, $match); }
				else{ $match = array(); }

				$db_html = $this->db->real_escape_string($test);
				$db_datetime_response = date("Y-m-d H:i:s");
				$db_response = $this->db->real_escape_string($response_field);

				if(sizeof($match) > 0){
					// Success (Status 3)
					$this->db->query("UPDATE scs SET status = '3', datetime_response = '{$db_datetime_response}', response = '{$db_response}', html = '{$db_html}' WHERE id = '{$campaign}'");
					$this->db->query("UPDATE scs SET seconds_total = UNIX_TIMESTAMP(datetime_response) - UNIX_TIMESTAMP(datetime_enter), seconds_setup = UNIX_TIMESTAMP(datetime_queue) - UNIX_TIMESTAMP(datetime_enter), seconds_queue = UNIX_TIMESTAMP(datetime_dispatch) - UNIX_TIMESTAMP(datetime_queue), seconds_user = UNIX_TIMESTAMP(datetime_response) - UNIX_TIMESTAMP(datetime_dispatch) WHERE id = '{$campaign}'");
					$response['correct'] = 1;
					$response['status'] = 1;
				}
				else{
					// Failure (Status 4)
					$this->db->query("UPDATE scs SET status = '4', datetime_response = '{$db_datetime_response}', response = '{$db_response}', html = '{$db_html}' WHERE id = '{$campaign}'");
					$response['correct'] = 0;
					$response['status'] = 1;
				}

			}

			else{

				$db_html = $this->db->real_escape_string($test);

				$this->db->query("UPDATE scs SET html = '{$db_html}' WHERE id = '{$campaign}'");

				$response['html'] = $test;
				$response['status'] = 1;

			}

			return $response;

		}

		function Test($url){

			// Test for presence of a captcha and return all of the page information

			$response = array(
				'html' => '',
				'status' => 0,
				'campaign' => 0,
				'campaign_url' => '',
				'error' => ''
			);

			while(1){
			
				$captcha = $this->GetCaptcha($url);
				$response['html'] = $captcha['html'];
				if($captcha['status'] == 0){ break; }

				$queryArray = array();
				foreach($captcha['data'] as $data){
					if($data['name'] == 'recaptcha_response_field'){ continue; }
					$queryArray[$data['name']] = $data['value'];
				}

				$response['captchaform'] = $captcha;

				// Add to database

				$db_keystring = $this->db->real_escape_string($captcha['key']);
				$db_challenge = $this->db->real_escape_string($captcha['challenge']);
				$db_url = $this->db->real_escape_string($url);
				$db_image = $this->db->real_escape_string($captcha['image']);
				$db_datetime_enter = date("Y-m-d H:i:s");
				$db_data = $this->db->real_escape_string(http_build_query($queryArray));
				$db_action = $this->db->real_escape_string($captcha['action']);
				$db_method = $this->db->real_escape_string($captcha['method']);
				$db_status = 0;
				$db_ready = $this->options['ready'];
				$db_tries = 0;
				$db_headers = $this->db->real_escape_string($this->options['headers']);
				$db_cookies = $this->db->real_escape_string($this->options['cookies']);
				$db_regex = $this->db->real_escape_string($this->options['regex']);

				if($this->options['campaign']){

					// Retry campaign (Status 0)
					$res = $this->db->query("UPDATE scs SET keystring = '{$db_keystring}', challenge = '{$db_challenge}', image = '{$db_image}', datetime_enter = '{$db_datetime_enter}', datetime_queue = NULL, datetime_dispatch = NULL, datetime_response = NULL, data = '{$db_data}', action = '{$db_action}', method = '{$db_method}', status = '0', ready = '" . $this->options['ready'] . "', tries = tries + 1 WHERE id = '" . $this->options['campaign'] . "'");

					$response['campaign'] = $this->options['campaign'];
					$response['status'] = 1;

				}

				else{

					// Create campaign (Status 0, at ready status defined by options)
					$res = $this->db->query("INSERT INTO scs (keystring, challenge, url, image, datetime_enter, data, regex, headers, cookies, action, method, status, tries, ready) VALUES ('{$db_keystring}', '{$db_challenge}', '{$db_url}', '{$db_image}', '{$db_datetime_enter}', '{$db_data}', '{$db_regex}', '{$db_headers}', '{$db_cookies}', '{$db_action}', '{$db_method}', '{$db_status}', '{$db_tries}', '{$db_ready}')");
					if($this->db->errno > 0) {
						$response['error'] = $this->db->error;
						break;
					}

					$response['campaign'] = $this->db->insert_id;
					$response['campaign_url'] = 'http://' . $this->hostName . '/scs.api.php?type=status&campaign=' . $this->db->insert_id;
					$response['status'] = 1;

				}

				break;

			}

			return $response;

		}

		function Download($url){

			$datafile = "downloads/data" . date("YmdHis") . md5($url);
			$outputfile = "downloads/output" . date("YmdHis") . md5($url);
			$command = 'curl --silent --output "' . $datafile . '" ' . $this->options['headers'] . ' ' . $this->options['postdata'] . ' "' . $url . '" > ' . $outputfile;

			shell_exec($command);

			$response = file_get_contents($datafile);

			$this->DeleteFile($datafile);
			$this->DeleteFile($outputfile);

			return $response;

		}

		function ProcessAttributes($element, $attributes){

			$response = array('template_text' => true, 'template_file' => false, 'template_valid' => true);

			foreach($attributes as $key => $val){

				$response[$val] = "";

				while(1){
					if(preg_match("/recaptcha_response_field/i", $element) == 1){ $response['template_valid'] = false; }
					preg_match("/{$val}(\ *?)=(\ *)(.*?)(?=(\ |\>))/i", $element, $match);
					if(sizeof($match) == 0){ break; }
					$match = $match[0];
					$match = explode("=", $match);
					$match = trim($match[1], "\"\'\ \t\n\r\0\x0B");
					$response[$val] = $match;
					break;
				}

				// Set templating options for mustache
				if(isset($response['type']) && preg_match("/file/i", $response['type']) == 1){ $response['template_text'] = false; $response['template_file'] = true; }

			}

			return $response;

		}

		function SetOptions($type, $s){

			if(isset($this->options[$type])){ $this->options[$type] = $s; }

		}

		function SubmitCaptchas($a){

			while(1){

				if(gettype($a) != "array" || sizeof($a) == 0 ){ return false; }
				if($a["status"] == 0){ return false; }

				return true;

			}

			return false;

		}

		function Update(){ // Server Update

			// Delete old live campaigns
			$res = $this->db->query("SELECT id FROM scs WHERE ready = 1 AND datetime_enter < NOW() - INTERVAL 24 HOUR");
			while($fld = $res->fetch_assoc()){
				$this->LogMessage($fld['id'] . ' is too old. Deleting...');
			}
			$this->db->query("DELETE FROM scs WHERE ready = 1 AND datetime_enter < NOW() - INTERVAL 24 HOUR");
			
			// Delete old non live campaigns
			$res = $this->db->query("SELECT id FROM scs WHERE ready = 0 AND datetime_enter < NOW() - INTERVAL 1 HOUR");
			while($fld = $res->fetch_assoc()){
				$this->LogMessage($fld['id'] . ' was not configured in time. Deleting...');
			}
			$this->db->query("DELETE FROM scs WHERE ready = 0 AND datetime_enter < NOW() - INTERVAL 1 HOUR");

			// Expire after 10 tries
			$res = $this->db->query("SELECT id FROM scs WHERE tries >= '10' AND status != '5'");
			while($fld = $res->fetch_assoc()){
				$this->LogMessage($fld['id'] . ' expired after 10 tries. Stashing...');
			}
			if($res->num_rows > 0){
				// Stash campaign (Status 5)
				$this->db->query("UPDATE scs SET status = '5' WHERE tries >= '10'");
			}

			// De-queue queued campaigns
			$res = $this->db->query("SELECT id FROM scs WHERE status = 1 AND datetime_queue < NOW() - INTERVAL 30 SECOND");
			while($fld = $res->fetch_assoc()){
				$this->LogMessage($fld['id'] . ' has been de-queued.');
			}
			$this->db->query("UPDATE scs SET status = 0 WHERE status = 1 AND datetime_queue < NOW() - INTERVAL 30 SECOND");

			// Retry failed records (Status 4 failed)
			$res = $this->db->query("SELECT id FROM scs WHERE status = '4'");
			while($fld = $res->fetch_assoc()){
				// $this->Test($fld["url"], $fld["regex"]);
				$this->LogMessage($fld['id'] . ' was not solved correctly. Retrying...');
				$command = 'scs.server.retry.php campaign=' . $fld['id'];
				$this->RunAsyncShellCommand($this->phpBin, $command);
			}

			// Retry old records (of status 0 - initiated by pageload)
			$res = $this->db->query('SELECT id FROM scs WHERE server_action = "retry"');
			while($fld = $res->fetch_assoc()) {
				$this->LogMessage($fld['id'] . ' was not dispatched in time. Retrying...');
				$command = 'scs.server.retry.php campaign=' . $fld['id'];
				$this->RunAsyncShellCommand($this->phpBin, $command);
			}
			$this->db->query('UPDATE scs SET server_action = NULL WHERE server_action = "retry"');

			// Retry old records (of status 2 - dispatched)
			$res = $this->db->query("SELECT id FROM scs WHERE status = 2 AND datetime_dispatch < NOW() - INTERVAL 30 SECOND");
			while($fld = $res->fetch_assoc()){
				// $this->Test($fld["url"], $fld["regex"]);
				$this->LogMessage($fld['id'] . ' expired. Retrying...');
				$command = 'scs.server.retry.php campaign=' . $fld['id'];
				$this->RunAsyncShellCommand($this->phpBin, $command);
			}

			// Notify successes!
			$res = $this->db->query('SELECT id, tries FROM scs WHERE status = 3 AND ready = 1');
			while($fld = $res->fetch_assoc()) {
				$this->LogMessage($fld['id'] . ' was successfully solved!');
			}
			$this->db->query('UPDATE scs SET ready = 0 WHERE status = 3');

			return true;

		}

		function LogMessage($message, $level = 0) {

			if($this->logLevel == 0) { return false; }

			echo(date('Y-m-d H:i:s') . ' - ' . $message . PHP_EOL);

			return true;

		}

		function SetLogLevel($level) {

			$this->logLevel = $level;

		}

	}

?>